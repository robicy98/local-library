const express = require('express');
const path = require('path');
const eformidable = require('express-formidable');
const db = require('../database/bookDbHandler');

const router = express.Router();

router.use(express.static(path.join(__dirname, 'static')));
const uploadDir = path.join(__dirname, '/file-uploads');

router.use('/', (req, res) => {
  eformidable({ uploadDir });
  const fileHandler = req.files.file;
  const data = {
    isbn: String(req.fields.isbn),
    title: String(req.fields.title),
    author: String(req.fields.author),
    year: parseInt(req.fields.year, 10),
    desc: String(req.fields.content),
    remainds: String(req.fields.nr),
    file: path.basename(fileHandler.path),
  };

  db.selectBook(data.isbn, (err, result) => {
    if (err) {
      const model = {
        status: 'Error 403',
        message: err,
      };
      res.status(403).render('message', model);
    } else if (result[0] === undefined) {
      db.insertBook(data, (error) => {
        if (!error) {
          const model = {
            status: '200 OK',
            message: 'Book added!',
          };
          res.status(200).render('message', model);
        } else {
          const model = {
            status: 'Error 403',
            message: error,
          };
          res.status(403).render('message', model);
        }
      });
    } else {
      db.updateBook(result[0].Remainders + data.nr, data.isbn, (uerr) => {
        if (!uerr) {
          const model = {
            status: '200 OK',
            message: 'Book updated!',
          };
          res.status(200).render('message', model);
        } else {
          const model = {
            status: 'Error 403',
            message: uerr,
          };
          res.status(403).render('message', model);
        }
      });
    }
  });
});

module.exports = router;
