const express = require('express');
const db = require('../database/userDbHandler');

const router = express.Router();

router.post('/', (req, resp) => {
  db.logOutMethod(req.fields.username, (err) => {
    if (err) {
      const model = {
        status: 'Error 403',
        message: `${err.message}`,
      };
      resp.status(403).render('message', model);
    } else {
      req.session.destroy();
      resp.redirect('/');
    }
  });
});

module.exports = router;
