const express = require('express');
const db = require('../database/userDbHandler');

const router = express.Router();

router.get('/', (req, res) => {
  const all = [];
  db.selectAll((err, result) => {
    if (err) {
      const model = {
        status: 'Error 403',
        message: err.message,
      };
      res.status(403).render('message', model);
    } else {
      let i = 0;
      while (result[i]) {
        all.push({
          username: result[i].username,
          email: result[i].email,
          isAdmin: result[i].isAdmin,
          isOnline: result[i].isOnline,
        });
        i += 1;
      }
      res.status(200).render('userlist', { user: all });
    }
  });
});

module.exports = router;
