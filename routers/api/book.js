const express = require('express');
const bookDB = require('../../database/bookDbHandler');
const { deleteFromStoreISBN } = require('../../database/storeDbHandler');

const router = express.Router();

router.get('/', (req, res) => {
  bookDB.selectAll((err, result) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json(result);
    }
  });
});

router.get('/:isbn', (req, res) => {
  const { isbn } = req.params;
  bookDB.selectBook(isbn, (err, result) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json(result);
    }
  });
});

router.delete('/:isbn', (req, res) => {
  const { isbn } = req.params;
  deleteFromStoreISBN(isbn, (err) => {
    if (err) {
      res.sendStatus(403);
    } else {
      bookDB.deleteBook(isbn, (err2) => {
        if (err2) {
          res.sendStatus(403);
        } else {
          res.sendStatus(200);
        }
      });
    }
  });
});

module.exports = router;
