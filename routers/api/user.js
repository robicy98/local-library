const ip = require('ip');
const crypto = require('crypto');
const fetch = require('node-fetch');
const express = require('express');
const userDB = require('../../database/userDbHandler');
const { PORT } = require('../../index');


const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;
const router = express.Router();

router.get('/', (req, res) => {
  userDB.selectAll((err, result) => {
    if (err) {
      console.error(err);
    } else {
      res.json(result);
    }
  });
});

router.get('/:userID', (req, res) => {
  const { userID } = req.params;
  if (parseInt(userID, 10)) {
    userDB.selectUserID(parseInt(userID, 10), (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  } else {
    userDB.selectUser(userID, (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  }
});


router.delete('/:userID', (req, res) => {
  const { userID } = req.params;
  if (parseInt(userID, 10)) {
    userDB.deleteUserID(parseInt(userID, 10), (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  } else {
    userDB.deleteUsername(userID, (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  }
});

function insert(data) {
  userDB.insertUser(data, (err) => {
    if (err) {
      throw new Error(err.message);
    }
  });
}

function update(data) {
  userDB.updateByID(data.id, data.username, data.email, data.password, (err) => {
    if (err) {
      throw new Error(err.message);
    }
  });
}

router.post('/', async (req, res) => {
  const {
    name, password, confirm, emailcim, adminCode,
  } = req.fields;
  let adminBool = 0;
  if (adminCode === '43KLJ1gPZTXPy4uU') {
    adminBool = 1;
  }
  if (password !== confirm) {
    res.status(403);
  }
  const user = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/user/${name}`)).json())[0];
  if (!user) {
    crypto.randomBytes(saltSize, (err, salt) => {
      if (err) {
        res.status(500).send(`Salt generation unsuccessful: ${err.message}`);
      }
      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
        if (cryptErr) {
          res.status(500).send(`Hashing unsuccessful: ${cryptErr.message}`);
        } else {
          const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
          const data = {
            username: name,
            email: emailcim,
            password: hashWithSalt,
            admin: adminBool,
          };
          try {
            insert(data);
          } catch (error) {
            res.status(403);
          }
        }
      });
    });
    res.status(200).render('home', {});
  }
});

router.put('/', async (req, res) => {
  console.log(req.fields);
  const {
    name, password, emailcim,
  } = req.fields;
  const user = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/user/${name}`)).json())[0];
  if (user) {
    crypto.randomBytes(saltSize, (err, salt) => {
      if (err) {
        res.status(500).send(`Salt generation unsuccessful: ${err.message}`);
      }
      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
        if (cryptErr) {
          res.status(500).send(`Hashing unsuccessful: ${cryptErr.message}`);
        } else {
          const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
          const data = {
            username: name,
            email: emailcim,
            password: hashWithSalt,
            id: user[0].UserID,
          };
          try {
            update(data);
          } catch (error) {
            res.status(403);
          }
        }
      });
    });
    const model = {
      status: '200 OK',
      message: 'Data modificated',
    };
    res.status(403).render('message', model);
  }
});

module.exports = router;
