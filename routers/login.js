const express = require('express');
const crypto = require('crypto');
const db = require('../database/userDbHandler');

const hashSize = 32,
  // saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;
const invalid = 'Invalid Username or password';
const loggedIn = 'User already logged in';

const router = express.Router();

function login(username, pass) {
  db.logInMethod(username, pass, (err, res) => {
    if (err) {
      throw new Error(err.message);
    } else if (res.affectedRows === 0) {
      throw new Error(invalid);
    } else if (res.changedRows === 0) {
      throw new Error(loggedIn);
    }
  });
}

router.post('/', (req, resp) => {
  const password = String(req.fields.password);
  const username = String(req.fields.username);
  let hashWithSalt;
  let usertype = 'user';

  db.selectUser(username, (err2, result) => {
    if (err2) {
      const model = {
        status: 'Error 403',
        message: err2.message,
      };
      resp.status(403).render('message', model);
    } else if (result[0].isAdmin) {
      usertype = 'admin';
    }
  });

  db.selectUser(username, (err, ress) => {
    if (ress) {
      hashWithSalt = ress[0].userPassword;
    }
    const expectedHash = hashWithSalt.substring(0, hashSize * 2),
      salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');

    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (errr, binaryHash) => {
      if (errr) {
        resp.status(500).send(`Hashing unsuccessful: ${errr.message}`);
      } else {
        const actualHash = binaryHash.toString('hex');

        if (expectedHash === actualHash) {
          try {
            login(username, hashWithSalt);
          } catch (error) {
            const model = {
              status: 'Error 403',
              message: error,
            };
            resp.status(403).render('message', model);
          }
          if (usertype === 'admin') {
            const model = {
              username: req.fields.username,
              username2: req.fields.username,
            };
            req.session.username = req.fields.username;
            req.session.admin = true;
            resp.status(200).render('loggedinadmin', model);
          } else {
            const model = {
              username: req.fields.username,
              username2: req.fields.username,
            };
            req.session.username = req.fields.username;
            req.session.admin = false;
            resp.status(200).render('loggedinuser', model);
          }
        } else {
          const model = {
            status: 'Error 403',
            message: 'Password do not match',
          };
          resp.status(403).render('message', model);
        }
      }
    });
  });
});

module.exports = router;
