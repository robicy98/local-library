const express = require('express');
const crypto = require('crypto');
const db = require('../database/userDbHandler');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const router = express.Router();

function insert(data) {
  db.insertUser(data, (err) => {
    if (err) {
      throw new Error(err.message);
    }
  });
}

function select(username) {
  db.selectUser(username, (err, result) => {
    if (err) {
      throw new Error(err.message);
    } else if (result.length > 0) {
      throw new Error('User already exists');
    }
  });
}

router.post('/', (req, res) => {
  let adminBool = 0;
  const username = String(req.fields.username);
  const password = String(req.fields.password);
  if (String(req.fields.admin) === '43KLJ1gPZTXPy4uU') {
    adminBool = 1;
  }
  if (String(req.fields.password) !== String(req.fields.confirm)) {
    const model = {
      status: 'Error 403',
      message: 'Password error',
    };
    res.status(403).render('message', model);
  }
  try {
    select(username);
  } catch (error) {
    const model = {
      status: 'Error 403',
      message: error,
    };
    res.status(403).render('message', model);
  }
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) {
      res.status(500).send(`Salt generation unsuccessful: ${err.message}`);
    }
    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        res.status(500).send(`Hashing unsuccessful: ${cryptErr.message}`);
      } else {
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        const data = {
          username: String(req.fields.username),
          email: String(req.fields.email),
          password: hashWithSalt,
          admin: adminBool,
        };
        try {
          insert(data);
        } catch (error) {
          const model = {
            status: 'Error 403',
            message: error,
          };
          res.status(403).render('message', model);
        }
      }
    });
  });
  res.redirect('/');
});

module.exports = router;
