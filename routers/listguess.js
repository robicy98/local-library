const express = require('express');
const db = require('../database/bookDbHandler');

const router = express.Router();

router.get('/', (req, res) => {
  const all = [];
  db.selectAll((err, result) => {
    if (err) {
      const model = {
        status: 'Error 403',
        message: err.message,
      };
      res.status(403).render('message', model);
    } else {
      let i = 0;
      while (result[i]) {
        if (result[i].Remainders !== 0) {
          all.push({
            author: result[i].Author,
            title: result[i].Title,
          });
        }
        i += 1;
      }
      res.status(200).render('guesslist', { book: all });
    }
  });
});

module.exports = router;
