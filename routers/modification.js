const express = require('express');

const router = express.Router();

router.get('/', (req, resp) => {
  resp.status(200).render('modif', { username: req.session.username });
});

module.exports = router;
