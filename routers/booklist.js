const express = require('express');
const db = require('../database/bookDbHandler');

const router = express.Router();

// 127.0.0.1:8080/listbooks
router.get('/', (req, res) => {
  res.status(200).render('booklist', { username: req.session.username });
});

// 127.0.0.1:8080/listbooks/all
router.get('/all', (req, res) => {
  const all = [];
  db.selectAll((err, result) => {
    if (err) {
      res.sendStatus(403);
    } else {
      let i = 0;
      while (result[i]) {
        all.push({
          author: result[i].Author,
          title: result[i].Title,
          isbnNr: result[i].ISBN,
          copies: result[i].Remainders,
          summary: result[i].Summary,
          releaseDate: result[i].Release_Date,
        });
        i += 1;
      }
      res.json(all);
    }
  });
});

router.get('/all/:search', (req, res) => {
  const all = [];
  const { search } = req.params;
  db.searchByTitle(search, (err, result) => {
    if (err) {
      res.sendStatus(403);
    } else {
      let i = 0;
      while (result[i]) {
        all.push({
          author: result[i].Author,
          title: result[i].Title,
          isbnNr: result[i].ISBN,
          copies: result[i].Remainders,
          summary: result[i].Summary,
          releaseDate: result[i].Release_Date,
        });
        i += 1;
      }
      res.json(all);
    }
  });
});

module.exports = router;
