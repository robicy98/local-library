const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  const { username } = req.session;
  const model = {
    user: username,
    all: 'true',
  };
  res.render('mes', model);
});

module.exports = router;
