const express = require('express');
const storeDB = require('../database/storeDbHandler');
const userDB = require('../database/userDbHandler');
const bookDB = require('../database/bookDbHandler');
const messageDB = require('../database/messageDbHandler');
const { borrow, giveBack } = require('../database/commonDB');

const router = express.Router();
// dbaccess/...

router.get('/ownedBy/:userName', (req, res) => {
  const { userName } = req.params;
  storeDB.selectByUsername(userName, (error, result) => {
    if (error) {
      console.error(error);
    }
    res.json(result);
  });
});

router.get('/ownedBooks/:isbn', (req, res) => {
  const { isbn } = req.params;
  storeDB.selectByBook(isbn, (error, result) => {
    if (error) {
      console.error(error);
    }
    res.json(result);
  });
});

router.get('/messages', (req, res) => {
  messageDB.selectAll((error, result) => {
    if (error) {
      res.status(403);
    } else {
      res.json(result);
    }
  });
});

router.post('/messages/:userID', (req, res) => {
  const { userID } = req.params;
  messageDB.sendMessage(userID, (error) => {
    if (error) {
      res.status(403);
    } else {
      res.status(200);
    }
  });
});

router.get('/messages/:userID', (req, res) => {
  const { userID } = req.params;
  messageDB.selectMy(userID, (error, result) => {
    if (error) {
      res.status(403);
    } else {
      res.json(result);
    }
  });
});

router.get('/user/:userName', (req, res) => {
  const { userName } = req.params;
  if (parseInt(userName, 10)) {
    userDB.selectUserID(parseInt(userName, 10), (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  } else {
    userDB.selectUser(userName, (err, result) => {
      if (err) {
        console.error(err);
      } else {
        res.json(result);
      }
    });
  }
});

router.get('/userFromSession', (req, res) => {
  const { username } = req.session;
  userDB.selectUser(username, (err, result) => {
    if (err) {
      console.error(err);
    } else {
      res.json(result);
    }
  });
});

router.get('/book/:isbn', (req, res) => {
  const { isbn } = req.params;
  bookDB.selectBook(isbn, (error, result) => {
    if (error) {
      console.error(error);
    }
    res.json(result);
  });
});

router.patch('/book/:isbn', async (req, res) => {
  const { isbn } = req.params;
  const { username } = req.session;
  await borrow(isbn, username);
  res.sendStatus(200);
});

router.patch('/book/:isbn/return', async (req, res) => {
  const { isbn } = req.params;
  const { username } = req.session;
  await giveBack(isbn, username);
  res.sendStatus(200);
});

router.patch('/user/admin/:userID', (req, res) => {
  const { userID } = req.params;
  userDB.makeAdmin(userID, (err) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.sendStatus(200);
    }
  });
});

router.patch('/user/notAdmin/:userID', (req, res) => {
  const { userID } = req.params;
  userDB.notAdmin(userID, (err) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.sendStatus(200);
    }
  });
});

module.exports = router;
