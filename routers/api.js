const express = require('express');

const user = require('./api/user');
const book = require('./api/book');
// const store = require('./api/store');

const router = express.Router();

router.use('/users', user);
router.use('/books', book);
// router.use('/store', store);

module.exports = router;
