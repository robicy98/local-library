const fetch = require('node-fetch');
const ip = require('ip');

const { updateBook } = require('./bookDbHandler');
const { insertBook, deleteFromStore } = require('./storeDbHandler');
const { PORT } = require('../index');

module.exports.borrow = async (isbn, username) => {
  const { UserID } = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/user/${username}`)).json())[0];
  const { Remainders } = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/book/${isbn}`)).json())[0];

  if (Remainders > 0) {
    updateBook(Remainders - 1, isbn, (error) => {
      if (error) {
        console.error(error);
      } else {
        insertBook(UserID, isbn, (err) => {
          if (err) {
            console.error(err);
          }
        });
      }
    });
  }
};

module.exports.giveBack = async (isbn, username) => {
  const { UserID } = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/user/${username}`)).json())[0];
  const { Remainders } = (await (await fetch(`http://${ip.address()}:${PORT}/dbaccess/book/${isbn}`)).json())[0];

  updateBook(Remainders + 1, isbn, (error) => {
    if (error) {
      console.error(error);
    } else {
      deleteFromStore(UserID, isbn, (err) => {
        if (err) {
          console.error(err);
        }
      });
    }
  });
};
