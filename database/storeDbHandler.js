const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'BooksDb',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'Admin123!',
});

exports.selectAll = (callback) => {
  const query = 'select * from Store';
  pool.query(query, callback);
};

exports.insertBook = (userid, isbn, callback) => {
  const query = `INSERT INTO Store(UserID, ISBN, Datum)
    VALUES (?, ?, NOW());`;
  pool.query(query, [userid, isbn], callback);
};

exports.selectByBook = (isbn, callback) => {
  const query = 'select * from Store where ISBN = ?';
  pool.query(query, [isbn], callback);
};

exports.deleteFromStore = (userid, isbn, callback) => {
  const query = 'delete from Store where ISBN = ? AND UserID = ?';
  pool.query(query, [isbn, userid], callback);
};

exports.deleteFromStoreISBN = (isbn, callback) => {
  const query = 'delete from Store where ISBN = ?';
  pool.query(query, [isbn], callback);
};

exports.selectByUser = (userid, callback) => {
  const query = 'select * from Store where UserID = ?;';
  pool.query(query, [userid], callback);
};

exports.selectByUsername = (username, callback) => {
  const query = 'select Store.ISBN from Store, Users where username = ? AND Store.UserID = Users.UserID;';
  pool.query(query, [username], callback);
};
