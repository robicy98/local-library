const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'BooksDb',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'Admin123!',
});

exports.selectAll = (callback) => {
  const query = 'select * from Books';
  pool.query(query, callback);
};

exports.searchByTitle = (search, callback) => {
  const query = `select * from Books where Title like '${search}%'`;
  pool.query(query, callback);
};

exports.selectBook = (isbn, callback) => {
  const query = 'select * from Books where ISBN = ?';
  pool.query(query, [isbn], callback);
};

exports.insertBook = (data, callback) => {
  const query = `INSERT INTO Books(ISBN, Title, Author, Release_Date, Summary, Remainders, FileName)
  VALUES (?, ?, ?, ?, ?, ?, ?);`;
  pool.query(query, [data.isbn, data.title, data.author, data.year,
    data.desc, data.remainds, data.file], callback);
};

exports.updateBook = (num, isbn, callback) => {
  const query = 'UPDATE Books SET Remainders = ? WHERE ISBN = ?';
  pool.query(query, [num, isbn], callback);
};

exports.deleteBook = (isbn, callback) => {
  const query = 'delete from Books where ISBN = ?';
  pool.query(query, [isbn], callback);
};
