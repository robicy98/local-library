const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'BooksDb',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'Admin123!',
});


exports.selectAll = (callback) => {
  const query = 'select * from Users';
  pool.query(query, callback);
};

exports.selectUser = (username, callback) => {
  const query = 'select * from Users where username = ?;';
  pool.query(query, [username], callback);
};

exports.selectUserID = (userID, callback) => {
  const query = 'select * from Users where UserID = ?;';
  pool.query(query, [userID], callback);
};

exports.insertUser = (data, callback) => {
  const query = `INSERT INTO Users(username, email, userPassword, isAdmin, isOnline)
    VALUES (?,?,?,?,?);`;
  pool.query(query, [data.username, data.email,
    data.password, data.admin, 0], callback);
};

exports.logInMethod = (username, password, callback) => {
  const query = 'UPDATE Users SET isOnline = TRUE WHERE username = ? AND userPassword = ?;';
  pool.query(query, [username, password], callback);
};

exports.logOutMethod = (username, callback) => {
  const query = 'UPDATE Users SET isOnline = FALSE WHERE username = ?;';
  pool.query(query, [username], callback);
};

exports.deleteUsername = (username, callback) => {
  const query = 'delete from Users where username = ?';
  pool.query(query, [username], callback);
};

exports.deleteUserID = (userID, callback) => {
  const query = 'delete from Users where UserID = ?';
  pool.query(query, [userID], callback);
};

exports.updateByID = (userID, username, email, password, callback) => {
  const query = 'update Users set username = ?, email = ?, userPassword = ? where UserID = ?';
  pool.query(query, [username, email, password, userID], callback);
};

exports.makeAdmin = (userID, callback) => {
  const query = 'update Users set isAdmin = TRUE where UserID = ?';
  pool.query(query, [userID], callback);
};

exports.notAdmin = (userID, callback) => {
  const query = 'update Users set isAdmin = FALSE where UserID = ?';
  pool.query(query, [userID], callback);
};
