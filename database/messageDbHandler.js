const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'BooksDb',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'Admin123!',
});

exports.selectAll = (callback) => {
  const query = 'select * from Messages';
  pool.query(query, callback);
};

exports.selectMy = (userID, callback) => {
  const query = 'select * from Messages where toUser = ?';
  pool.query(query, [userID], callback);
};

exports.sendMessage = (userID, callback) => {
  const message = 'Please returne the book(s)!';
  const query = `insert into Messages (toUser, message) values (?, '${message}');`;
  console.log(query);
  pool.query(query, [userID], callback);
};
