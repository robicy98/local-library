// eslint-disable-next-line no-unused-vars
async function fillMessage() {
  const username = document.getElementById('user').innerText;
  const all = document.getElementById('all').innerText;
  let messages;
  if (all === 'false') {
    const userList = await (await fetch(`api/users/${username}`)).json();
    messages = await (await fetch(`/dbaccess/messages/${userList[0].UserID}`)).json();
  } else {
    messages = await (await fetch('/dbaccess/messages')).json();
  }
  messages.forEach(async (mess) => {
    const user = await (await fetch(`/dbaccess/user/${mess.toUser}`)).json();
    const elem = ` <li id="listitem">
      <p>To ${user[0].username}:</p>
      <p>${mess.message}</p>
    </li>`;
    document.getElementById('allMessage').insertAdjacentHTML('beforeend', elem);
  });
}
