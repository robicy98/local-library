// eslint-disable-next-line no-unused-vars
function showInfo(isbn) {
  document.getElementById(`extra_${isbn}`).hidden = false;
}

// eslint-disable-next-line no-unused-vars
function showSummary(isbn) {
  document.getElementById(`summary_${isbn}`).hidden = false;
}

// eslint-disable-next-line no-unused-vars
function hide(id) {
  document.getElementById(id).hidden = true;
}
// eslint-disable-next-line no-unused-vars
function visible(id) {
  document.getElementById(id).hidden = false;
}

// eslint-disable-next-line no-unused-vars
async function getBook(isbn) {
  await fetch(`/dbaccess/book/${isbn}`, {
    method: 'PATCH',
  });
  document.getElementById(`return_${isbn}`).hidden = false;
  document.getElementById(`borrow_${isbn}`).hidden = true;
  document.getElementById(`copies_${isbn}`).innerText = parseInt(document.getElementById(`copies_${isbn}`).innerText, 10) - 1;
}

// eslint-disable-next-line no-unused-vars
async function returnBook(isbn) {
  await fetch(`/dbaccess/book/${isbn}/return`, {
    method: 'PATCH',
  });
  document.getElementById(`return_${isbn}`).hidden = true;
  document.getElementById(`borrow_${isbn}`).hidden = false;
  document.getElementById(`copies_${isbn}`).innerText = parseInt(document.getElementById(`copies_${isbn}`).innerText, 10) + 1;
}

// eslint-disable-next-line no-unused-vars
async function messageToUsers(isbn) {
  const users = await (await fetch(`/dbaccess/ownedBooks/${isbn}`)).json();
  users.forEach(async (user) => {
    await fetch(`/dbaccess/message/${user.UserID}`, {
      method: 'POST',
    });
  });
}
// eslint-disable-next-line no-unused-vars
async function seraching() {
  const text = document.getElementById('search').value;
  const allBooks = await (await fetch('/listbooks/all')).json();
  allBooks.forEach((book) => {
    if (book.title.substring(0, text.length).toLowerCase() !== text.toLowerCase()) {
      hide(`li_${book.isbnNr}`);
    } else {
      visible(`li_${book.isbnNr}`);
    }
  });
}

// eslint-disable-next-line no-unused-vars
async function deleteBook(isbn) {
  console.log(typeof isbn);
  await fetch(`/api/books/${isbn}`, {
    method: 'DELETE',
  });
  hide(`li_${isbn}`);
}

// eslint-disable-next-line no-unused-vars
async function loadAllBooks() {
  const allBooks = await (await fetch('/listbooks/all')).json();
  const username = document.getElementById('username').innerText;
  const user = await (await fetch(`api/users/${username}`)).json();
  const owned = await (await fetch(`/dbaccess/ownedBy/${username}`)).json();
  allBooks.forEach((book) => {
    const elem = ` <li id=li_${book.isbnNr}>
    <div class="uk-card uk-card-secondary">
    <ul>
      <li onclick="showInfo('${book.isbnNr}')">ISBN: ${book.isbnNr}</li>
      <li>Szerzo: ${book.author}</li>
      <li onclick="showSummary('${book.isbnNr}')">Cim: ${book.title}</li>
      <ul onclick="hide('summary_${book.isbnNr}')" id="summary_${book.isbnNr}" hidden><li>Summary: ${book.summary}</li></ul>
      <li>Peldanyok: <b id="copies_${book.isbnNr}">${book.copies}</b></li>
      <ul id="extra_${book.isbnNr}" hidden>
        <div id="remaind_${book.isbnNr}" ${(book.copies === 0) ? 'hidden' : ''}>
          <li id="return_${book.isbnNr}" ${(!owned.some(value => value.ISBN === book.isbnNr)) ? 'hidden' : ''}><button onclick="returnBook('${book.isbnNr}')">Return this Book!</button></li>
          <li id="borrow_${book.isbnNr}" ${(owned.some(value => value.ISBN === book.isbnNr)) ? 'hidden' : ''}><button onclick="getBook('${book.isbnNr}')">Get this Book!</button></li>
        </div>
          <button id="empty_${book.isbnNr}" onclick="messageToUsers('${book.isbnNr}')" ${(book.copies === 0) ? '' : 'hidden'}>Sent message to returne this book!</button></li>
      </ul>
      <li  ${(!user[0].isAdmin) ? 'hidden' : ''} >
        <button id="delete_${book.isbnNr}" onclick="deleteBook('${book.isbnNr}')">Delete this book</button>
      </li>
    </ul>
    </div>
  </li>`;

    document.getElementById('allbooks').insertAdjacentHTML('beforeend', elem);
  });
}
