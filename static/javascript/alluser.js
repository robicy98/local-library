// eslint-disable-next-line no-unused-vars
function hide(id) {
  document.getElementById(id).hidden = true;
}

// eslint-disable-next-line no-unused-vars
function visible(id) {
  document.getElementById(id).hidden = false;
}

// eslint-disable-next-line no-unused-vars
async function admin(id) {
  await fetch(`/dbaccess/user/admin/${id}`, {
    method: 'PATCH',
  });
  hide(`notadmin_${id}`);
  visible(`admin_${id}`);
  document.getElementById(`isadmin_${id}`).innerText = 'Is Admin: Yes';
}

// eslint-disable-next-line no-unused-vars
async function notadmin(id) {
  await fetch(`/dbaccess/user/notAdmin/${id}`, {
    method: 'PATCH',
  });
  hide(`admin_${id}`);
  visible(`notadmin_${id}`);
  document.getElementById(`isadmin_${id}`).innerText = 'Is Admin: No';
}
// eslint-disable-next-line no-unused-vars
async function searcuser() {
  const text = document.getElementById('name').value;
  const allUser = await (await fetch('/api/users')).json();
  allUser.forEach((user) => {
    if (user.username.substring(0, text.length).toLowerCase() !== text.toLowerCase()) {
      hide(`user_${user.UserID}`);
    } else {
      visible(`user_${user.UserID}`);
    }
  });
}

// eslint-disable-next-line no-unused-vars
async function loadAllUser() {
  const allUser = await (await fetch('/api/users')).json();
  allUser.forEach((user) => {
    const elem = ` <li id="user_${user.UserID}">
    <ul>
      <li >Username: ${user.username}</li>
      <li >Email: ${user.email}</li>
      <li id="isadmin_${user.UserID}">Is Admin: ${(user.isAdmin) ? 'Yes' : 'No'} </li>
      <li >Is Online: ${(user.isOnline) ? 'Yes' : 'No'} </li>
      <button onclick="admin(${user.UserID})" id="notadmin_${user.UserID}" ${(user.isAdmin) ? 'hidden' : ''}>Insert to Admins</button>
      <button onclick="notadmin(${user.UserID})" id="admin_${user.UserID}" ${(!user.isAdmin) ? 'hidden' : ''}>Delete from Admins</button>
    </ul>
  </li>`;

    document.getElementById('allusers').insertAdjacentHTML('beforeend', elem);
  });
}

// eslint-disable-next-line no-unused-vars
async function modLoad() {
  const names = document.getElementById('name');
  names.value = document.getElementById('username').innerText;
}

// eslint-disable-next-line no-unused-vars
async function putData() {
  await fetch('/api/users', {
    method: 'PUT',
    fields: {
      name: document.getElementById('name').value,
      emailcim: document.getElementById('emailcim').value,
      password: document.getElementById('password').value,
    },
  });
}
