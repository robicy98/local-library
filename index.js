const express = require('express');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const eformidable = require('express-formidable');
const fs = require('fs');
const session = require('express-session');

const PORT = 8080;
exports.PORT = PORT;

const staticDir = path.join(__dirname, 'static');
const uploadDir = path.join(__dirname, 'file-uploads');
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

const app = express();

const logoutRouter = require('./routers/logout');
const loginRouter = require('./routers/login');
const singinRouter = require('./routers/singin');
const bookListRouter = require('./routers/booklist');
const newBook = require('./routers/newbook');
const addBook = require('./routers/add');
const userListRouter = require('./routers/listusers');
const guessListRouter = require('./routers/listguess');
const dbAccess = require('./routers/dbaccess');
const api = require('./routers/api');
const allmessage = require('./routers/allmessage');
const mymessage = require('./routers/mymessage');
const modif = require('./routers/modification');

// Initialize Session
app.use(session({
  secret: 'deadb3efbadc0de',
  resave: false,
  saveUninitialized: true,
}));

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({
  extended: true,
}));

app.use(express.static(staticDir));
app.use(eformidable({ uploadDir }));

app.use('/login', loginRouter);
app.use('/logout', logoutRouter);
app.use('/singin', singinRouter);
app.use('/listbooks', bookListRouter);
app.use('/newbook', newBook);
app.use('/add', addBook);
app.use('/listusers', userListRouter);
app.use('/listg', guessListRouter);
app.use('/dbaccess', dbAccess);
app.use('/api', api);
app.use('/allmessage', allmessage);
app.use('/mymessage', mymessage);
app.use('/modification', modif);
app.get('/guess', (req, res) => {
  res.status(200).render('loggedinguess', {});
});
app.get('/*', (req, res) => {
  const model = {
    status: '404 Page not Found',
    message: '',
  };
  res.status(404).render('message', model);
});


app.listen(PORT, () => {
  console.log('Server listening in localhost:8080...');
});
